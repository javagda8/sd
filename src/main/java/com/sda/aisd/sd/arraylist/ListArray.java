package com.sda.aisd.sd.arraylist;

import java.util.Arrays;

public class ListArray {

    private Object[] array;
    private int numberOfElements;

    public ListArray(int initialSize) {
        this.array = new Object[initialSize];
        this.numberOfElements = 0;
    }

    public ListArray() {
        this(10);
    }

    public void add(Object o) {
        if (numberOfElements >= array.length) {
            extendArray();
        }
        array[numberOfElements++] = o;
    }

    private void extendArray() {
        // alokuje sobie 2 razy wiecej (40000)
        Object[] newArray = new Object[array.length * 2]; // rozszerzamy 2 razy

        // przepisuje stare rekordy (20000)
        for (int i = 0; i < numberOfElements; i++) {
            newArray[i] = array[i];
        }

        // zastepuje stara tablice nową
        array = newArray;
    }

    public void removeLast() {
        numberOfElements--;
//        array[numberOfElements] = null;
    }

    public Object get(int i) {
        if (i > numberOfElements) {
            throw new ArrayIndexOutOfBoundsException();
        } else if (i < 0) {
            throw new ArrayIndexOutOfBoundsException();
        }
        return array[i];
    }

    public void removeAt(int i) {
        numberOfElements--;

        for (int j = i; j < numberOfElements; j++) {
            array[j] = array[j + 1];
        }
    }

    public void addAt(int i, Object o) {
        // rozszerzamy tablice
        if (numberOfElements >= array.length) {
            extendArray();
        }
        int j;
        for (j = numberOfElements - 1; j >= i; j--) {
            System.out.println("Przestawiam: " + j + " na " + (j + 1));
            array[j + 1] = array[j];
        }

        numberOfElements++;

        array[i] = o;
    }

    @Override
    public String toString() {
        return "ListArray{" +
                "array=" + Arrays.toString(array) +
                '}';
    }


    public int size() {
        return numberOfElements;
    }

    public int getCapacity() {
        return array.length;
    }

// Implementacja metod podstawowych:
    // - get ( i )
    // - remove - z końca
    // - add
    // - size

// Implementacja metod dodawania/usuwania na pozycji:
    // remove ( i )
    // add ( i )

//* Zmiana klasy na klasę generyczną (Zamiast 'Object')
}
