package com.sda.aisd.sd.arraylist;

import java.util.ArrayList;

public class MainListsArray {
    public static void main(String[] args) {
        ArrayList<Integer> arrayList = new ArrayList<>();



        ListArray listArray = new ListArray();
        listArray.add(1);
        listArray.add(2);
        listArray.add(3);
        listArray.add(4);
        listArray.addAt(2, 5);
        System.out.println(listArray.toString());

        System.out.println(listArray.size());


        ListArrayGeneric<Integer> arrayGeneric = new ListArrayGeneric<>(Integer.class, 200);
        arrayGeneric.add(23);

    }
}
