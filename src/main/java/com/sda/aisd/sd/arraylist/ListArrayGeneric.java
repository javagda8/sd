package com.sda.aisd.sd.arraylist;

import java.lang.reflect.Array;
import java.util.Arrays;

public class ListArrayGeneric<T> {

    private T[] array;
    private int numberOfElements;

    public ListArrayGeneric(Class<?> claz, int initialSize) {
        this.array = (T[]) Array.newInstance(claz.getClass(), initialSize);
//        this.array = new T[initialSize];
        this.numberOfElements = 0;
    }

    public ListArrayGeneric() {
        this(Object.class, 10);
    }

    public void add(T o) {
        if (numberOfElements >= array.length) {
            extendArray();
        }
        array[numberOfElements++] = o;
    }

    private void extendArray() {
        // alokuje sobie 2 razy wiecej (40000)
        T[] newArray = (T[]) Array.newInstance(array[0].getClass(), array.length * 2);

        // przepisuje stare rekordy (20000)
        for (int i = 0; i < numberOfElements; i++) {
            newArray[i] = array[i];
        }

        // zastepuje stara tablice nową
        array = newArray;
    }

    public void removeLast() {
        numberOfElements--;
//        array[numberOfElements] = null;
    }

    public Object get(int i) {
        if (i > numberOfElements) {
            throw new ArrayIndexOutOfBoundsException();
        } else if (i < 0) {
            throw new ArrayIndexOutOfBoundsException();
        }
        return array[i];
    }

    public void removeAt(int i) {
        numberOfElements--;

        for (int j = i; j < numberOfElements; j++) {
            array[j] = array[j + 1];
        }
    }

    public void addAt(int i, T o) {
        // rozszerzamy tablice
        if (numberOfElements >= array.length) {
            extendArray();
        }
        int j;
        for (j = numberOfElements - 1; j >= i; j--) {
            System.out.println("Przestawiam: " + j + " na " + (j + 1));
            array[j + 1] = array[j];
        }

        numberOfElements++;

        array[i] = o;
    }

    @Override
    public String toString() {
        return "ListArray{" +
                "array=" + Arrays.toString(array) +
                '}';
    }


    public int size() {
        return numberOfElements;
    }

    public int getCapacity() {
        return array.length;
    }

// Implementacja metod podstawowych:
    // - get ( i )
    // - remove - z końca
    // - add
    // - size

// Implementacja metod dodawania/usuwania na pozycji:
    // remove ( i )
    // add ( i )

//* Zmiana klasy na klasę generyczną (Zamiast 'Object')
}
