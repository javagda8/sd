package com.sda.aisd.sd.linkedlist;

public class Node<T> {
    private Node<T> next;
    private Node<T> prev;
    private T data;

    public Node(T data) {
        this.data = data;

        this.next = this.prev = null;
    }

    public Node getNext() {
        return next;
    }

    public void setNext(Node next) {
        this.next = next;
    }

    public Node getPrev() {
        return prev;
    }

    public void setPrev(Node prev) {
        this.prev = prev;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}
