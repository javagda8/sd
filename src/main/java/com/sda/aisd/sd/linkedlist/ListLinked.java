package com.sda.aisd.sd.linkedlist;

public class ListLinked<T> {
    private Node<T> head;
    private Node<T> tail;

    private int size;

    public ListLinked() {
        this.head = null;
        this.tail = null;

        this.size = 0;
    }

    // add
    public void add(T o) {
        Node<T> newNode = new Node<>(o);
        if (head == null && tail == null) { // lista jest pusta
            head = newNode;
            tail = newNode;
        } else {
            newNode.setPrev(tail);
            tail.setNext(newNode);
            tail = newNode;
        }
    }

    public void print() {
        Node<T> tmp = head;

//przed:        System.out.println(tmp.getData());
        while (tmp != null) {
            System.out.println(tmp.getData()); // po
            tmp = tmp.getNext();
//przed:            System.out.println(tmp.getData());
        }
        System.out.println();
    }

    // remove
    public void remove() {
        if (head == null && tail == null) {
            return;
        } else if (head == tail) {
            head = null;
            tail = null;
        } else if (head != tail) {
            // tail - usunac
            Node poprzednikTail = tail.getPrev();
            poprzednikTail.setNext(null);
            tail = poprzednikTail;
        } else {
            System.out.println("Something is wrong.");
        }
    }

    // remove at position
    public void remove(int i) throws ArrayIndexOutOfBoundsException {
        int counter = 0;
        Node<T> tmp = head;

        // counter zlicza nam numer elementu w którym jesteśmy
        // i - jest numerem elementu do którego próbujemy dotrzeć
        // tmp != null - jest zabezpieczeniem przed NullPointerException
        while (counter != i && tmp != null) {
            tmp = tmp.getNext();
            counter++;
        }

        if (counter == i && tmp != null) { // dotarlismy do elementu i jest tam jakis obiekt
            Node poprzednik = tmp.getPrev();
            Node nastepnik = tmp.getNext();

            if (nastepnik != null) { // zabezpieczenie przed usunieciem ostatniego elementu
                nastepnik.setPrev(poprzednik);
            }
            if (poprzednik != null) { // zabezpieczenie przed usunieciem pierwszego elementu
                poprzednik.setNext(nastepnik);
            }

            if (tmp == head) { // jeśli usuwałem head
                head = nastepnik;
            }
            if (tmp == tail) { // jeśli usuwałem tail
                tail = poprzednik;
            }
        } else if (counter == i && tmp == null) {
            throw new ArrayIndexOutOfBoundsException();
        } else if (counter != i) {
            throw new ArrayIndexOutOfBoundsException();
        }
        // wyjście z pętli oznacza że
        // 1. counter == i && tmp != null - czyli dotarliśmy do odpowiedniego elementu
        // 2. counter == i && tmp == null
        // 3. counter != i - brakuje elementów (petla zostala zakonczona poniewaz tmp==null)
        //
    }

    // add at position
    public void add(int i, T o) {
        if (head == null && tail == null) {
            add(o);
            return;
        }

        Node<T> newNode = new Node<>(o);
        Node<T> tmp = head;

        int counter = 0;
        while (counter != i && tmp != null) {
            tmp = tmp.getNext();
            counter++;
        }

        if (counter == i && tmp != null) { // dotarlismy do odpowiedniego elementu
            Node nastepnikNowego = tmp;
            Node poprzednikNowego = tmp.getPrev();

            newNode.setNext(nastepnikNowego);
            newNode.setPrev(poprzednikNowego);

            if (nastepnikNowego != null) {
                nastepnikNowego.setPrev(newNode);
            }
            if (poprzednikNowego != null) {
                poprzednikNowego.setNext(newNode);
            }

            if (tmp == head) {
                head = newNode;
            }
        } else if (counter == i && tmp == null) { // chcemy dodać na samym końcu
            tail.setNext(newNode); // dodanie na koniec
            newNode.setPrev(tail); //
            tail = newNode; // podmiana tail
        }
    }


    // + remove object
    // get
    public T get(int i) {
        Node<T> tmp = head;

        int counter = 0;
        while (counter != i && tmp != null) {
            tmp = tmp.getNext();
            counter++;
        }
        if (counter == i && tmp != null) { // dotarlismy do elementu i jest tam jakis obiekt
            return tmp.getData();
        } else if (counter == i && tmp == null) {
            throw new ArrayIndexOutOfBoundsException();
        } else if (counter != i) {
            throw new ArrayIndexOutOfBoundsException();
        }
        System.err.println("Something is wrong, shouldn't have happened.");
        return null;
    }

    // size
    public int size() {
        Node<T> tmp = head;
        int counter = 0;

        while (tmp != null) {
            tmp = tmp.getNext();
            counter++;
        }

        return counter;
    }

}
