package com.sda.aisd.sd.linkedlist;

import java.util.LinkedList;

public class LinkedListTester {
    public static void main(String[] args) {
//        LinkedList<Integer> t = new LinkedList<>();
//        t.add(1);
//        t.add("string");
//        System.out.println(t.getClass().toString()); // Class<?>

        ListLinked<Integer> listLinked = new ListLinked<>();

        listLinked.remove();

        listLinked.add(1);
//        listLinked.remove(0);
        listLinked.add(2); // 0
        listLinked.add(3); // 1
        listLinked.add(4); // 2
//        System.out.println(listLinked.get(0));
//        System.out.println(listLinked.get(2));
//        System.out.println();
//        System.out.println(listLinked.size());
//        System.out.println();
//
//        listLinked.print();
//        listLinked.remove(2);
//        listLinked.add(5);
//        listLinked.add(6);
//        listLinked.add(7);
//
//        listLinked.print();
//        listLinked.remove(0);


        listLinked.print();
//        System.out.println(listLinked.size());

        listLinked.add(0, 100);
        listLinked.print();
        listLinked.add(4, 110);
        listLinked.print();

//        listLinked.remove(6);
//        listLinked.print();

    }
}
